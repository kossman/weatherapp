package com.example.dmitriykosse.cogniance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.dmitriykosse.cogniance.Models.HistoryData;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;

public class HistoryActivity extends AppCompatActivity {

    public HistoryData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        final ObjectMapper mapper = new ObjectMapper();
        RequestQueue queue = Volley.newRequestQueue(this);
        Long start = System.currentTimeMillis() - 86400000;
        Long end = System.currentTimeMillis();

        String request = WeatherApi.getWeatherApiHistoryExample;
        Log.d("DEBUG", request);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, request, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            data = mapper.readValue(response.toString().getBytes(), HistoryData.class);
                            Log.d("DEBUG", data.toString());
                        }
                        catch (IOException e) {
                            Log.e("ERROR", e.toString());
                        }
                        return;
                    }},

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getBaseContext(), "Network error occured", Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }
                }
        );

        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);

    }
}
