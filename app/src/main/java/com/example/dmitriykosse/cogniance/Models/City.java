package com.example.dmitriykosse.cogniance.Models;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Dmitriy.Kosse on 12.07.2017.
 */

public class City extends RealmObject {


    @PrimaryKey
    @Required // optional, but recommended.
    public Integer id;

    @Index
    @Required
    public String cityName;

    @Required
    public Integer apiCityID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Integer getApiCityID() {
        return apiCityID;
    }

    public void setApiCityID(Integer apiCityID) {
        this.apiCityID = apiCityID;
    }
}
