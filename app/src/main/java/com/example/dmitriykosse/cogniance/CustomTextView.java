package com.example.dmitriykosse.cogniance;

/**
 * Created by Dmitriy.Kosse on 10.07.2017.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomTextView extends AppCompatTextView {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public CustomTextView(Context context) {
        super(context);
        init(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            init(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs,
                          int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        int textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", 0);
        this.setTextSize(35);
        this.setPadding(10, 15, 10, 15);
        this.setTypeface(selectTypeface(context, textStyle));
    }


    private Typeface selectTypeface(Context context, int textStyle) {
        return FontManager.getInstance().getTypeface(context, textStyle);
    }


    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}


