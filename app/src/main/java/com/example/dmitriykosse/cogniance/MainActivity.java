package com.example.dmitriykosse.cogniance;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.activeandroid.ActiveAndroid;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.dmitriykosse.cogniance.Models.CacheAPI;
import com.example.dmitriykosse.cogniance.Models.City;
import com.facebook.stetho.Stetho;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import io.realm.Realm;
import io.realm.RealmQuery;

public class MainActivity extends AppCompatActivity {

    private TextView viewHeader, viewTemperature, viewPressure, viewHunidity, viewCondition;
    private Resources res;
    private ImageView headerCityLogo;
    private RequestQueue queue;
    private Integer selectedCity;
    private FloatingActionButton fab;

    private Realm realm;

    private ArrayList<String> forCache;
    private SharedPreferences preferences;

    private final String CACHE_COUNT_KEY = "CacheCount";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_kyiv:
                    checkCache(WeatherApi.cityIdKyiv);
                    return true;
                case R.id.navigation_london:
                    checkCache(WeatherApi.cityIdLondon);
                    return true;
                case R.id.navigation_newyork:
                    checkCache(WeatherApi.cityIdNewYork);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this); // For debugging form Chrome
        // Initialize Realm. Should only be done once when the application starts.
        Realm.init(this);
        realm = Realm.getDefaultInstance();

        InitTables.initCitiesItems(realm);

        if (savedInstanceState != null) {
            selectedCity = savedInstanceState.getInt("selectedCity");
            Log.d("DEBUG SavInstState", Integer.toString(selectedCity));
        }
        else {
            selectedCity = WeatherApi.cityIdKyiv;
        }

        setContentView(R.layout.activity_main_new);
        res = getResources();
        preferences = getPreferences(MODE_PRIVATE);

        // Create cache and network objects for volley library
        Cache cache = new DiskBasedCache(getCacheDir(), 1024*1024);
        Network network = new BasicNetwork(new HurlStack());
        queue = new RequestQueue(cache, network);
        queue.start();


        // Find all required Views
        viewHeader = (TextView) findViewById(R.id.header);
        viewTemperature = (TextView) findViewById(R.id.textViewTemperature);
        viewPressure = (TextView) findViewById(R.id.textViewPressure);
        viewHunidity = (TextView) findViewById(R.id.textViewHumidity);
        viewCondition = (TextView) findViewById(R.id.textViewMain);
        headerCityLogo = (ImageView) findViewById(R.id.city_logo);
        // Find data in cache or make network request to API
        checkCache(selectedCity);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    protected void requestWeatherApi(final Integer cityId) {

        String request = String.format(WeatherApi.weatherApiBaseUrl, cityId, WeatherApi.appid);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, request, null,
                new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    selectedCity = cityId;
                    forCache = contentInflater(response);
                    updateCache(cityId, forCache);
                    return;
                }},

                new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getBaseContext(), "Network error occured", Toast.LENGTH_LONG).show();
                        }
                    });
                    return;
                }
            }
        );

        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }

    protected ArrayList<String> contentInflater(JSONObject jsonObject) {
        try {
            JSONObject _main = jsonObject.getJSONObject("main");
            JSONObject _weather = jsonObject.getJSONArray("weather").getJSONObject(0);
            String cityName = jsonObject.getString("name");

            forCache = new ArrayList<>();

            viewHeader.setText(cityName);
            viewTemperature.setText(String.format(res.getString(R.string.content_temperature),_main.getString("temp")));
            viewPressure.setText(String.format(res.getString(R.string.content_pressure),_main.getString("pressure")));
            viewHunidity.setText(String.format(res.getString(R.string.content_humidity),_main.getString("humidity")));
            viewCondition.setText(String.format(res.getString(R.string.content_main),_weather.getString("main")));

            forCache.add(0, _main.getString("temp"));
            forCache.add(1, _main.getString("pressure"));
            forCache.add(2, _main.getString("humidity"));
            forCache.add(3, _weather.getString("main"));

            switch (cityName){
                case "Kiev":
                    Picasso.with(this).load(R.mipmap.ic_kyiv).into(headerCityLogo);
                    return forCache;
                case "London":
                    Picasso.with(this).load(R.mipmap.ic_london_logo).into(headerCityLogo);
                    return forCache;
                case "New York":
                    Picasso.with(this).load(R.mipmap.ic_new_york_logo).into(headerCityLogo);
                    return forCache;
            }

        }
        catch (JSONException e) {

            Log.e("ERROR", e.toString());
        }
        return forCache;
    }

    protected void contentInflater(CacheAPI cacheItem) {

        Log.d("DEBUG ACTUAL CACHE", cacheItem.city.cityName);
        viewHeader.setText(cacheItem.city.cityName);
        viewTemperature.setText(String.format(res.getString(R.string.content_temperature), cacheItem.temperature));
        viewPressure.setText(String.format(res.getString(R.string.content_pressure), cacheItem.pressure));
        viewHunidity.setText(String.format(res.getString(R.string.content_humidity), cacheItem.humidity));
        viewCondition.setText(String.format(res.getString(R.string.content_main), cacheItem.condition));

        switch (cacheItem.city.cityName){
            case "Kiev":
                Picasso.with(this).load(R.mipmap.ic_kyiv).into(headerCityLogo);
                return;
            case "London":
                Picasso.with(this).load(R.mipmap.ic_london_logo).into(headerCityLogo);
                return;
            case "New York":
                Picasso.with(this).load(R.mipmap.ic_new_york_logo).into(headerCityLogo);
                return;
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedCity", selectedCity);
    }

    protected void checkCache(Integer city_id){

        int cacheCount = preferences.getInt(CACHE_COUNT_KEY, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(CACHE_COUNT_KEY, cacheCount + 1);
        editor.apply();

        City city = realm.where(City.class).equalTo("apiCityID", city_id).findFirst();
        Log.d("DEBUG", city.toString());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        CacheAPI cacheItem = realm.where(CacheAPI.class).equalTo("city.apiCityID", city_id).findFirst();

        // Check if valid data present in cache
        if (cacheItem != null && (cacheItem.expiresTime > timestamp.getTime())) {
            //Log.d("DEBUG ACTUAL CACHE", cacheItem.toString());
            contentInflater(cacheItem);
            selectedCity = city_id;
            return;
        }
        else {
            //Log.d("DEBUG", "No cache for city_id: " + city_id);
            requestWeatherApi(city_id);
        }
    }

    protected void updateCache(Integer city_id, ArrayList<String> values) {  // values: Temp Press Hum Cond
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        CacheAPI cacheAPI;
        realm.beginTransaction();
        if (realm.where(CacheAPI.class).equalTo("city.apiCityID", city_id).findFirst() == null) {
            Log.d("DEBUG NO ITEM IN CACHE", values.toString());
            // No items exist

            Number currentIdNum = realm.where(CacheAPI.class).max("id");
            int nextId;
            if (currentIdNum == null) {
                nextId = 1;
            } else {
                nextId = currentIdNum.intValue() + 1;
            }

            cacheAPI = realm.createObject(CacheAPI.class, nextId);
            cacheAPI.city = realm.where(City.class).equalTo("apiCityID", city_id).findFirst();
        }
        else {
            Log.d("DEBUG EXPIRED CACHE", values.toString());
            // Cache Item is already exist in table
            cacheAPI = realm.where(CacheAPI.class).equalTo("city.apiCityID", city_id).findFirst();
        }

        cacheAPI.temperature = values.get(0);
        cacheAPI.pressure = values.get(1);
        cacheAPI.humidity = values.get(2);
        cacheAPI.condition = values.get(3);
        cacheAPI.expiresTime = timestamp.getTime() + 600000; // expiresTime 10 min
        realm.commitTransaction();
        Log.d("DEBUG: SAVE TO CACHE", cacheAPI.toString());
    }
}
