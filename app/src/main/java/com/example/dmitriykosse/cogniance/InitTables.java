package com.example.dmitriykosse.cogniance;

import android.util.Log;

import com.example.dmitriykosse.cogniance.Models.City;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Dmitriy.Kosse on 11.07.2017.
 */

public class InitTables {

    public static void initCitiesItems(Realm realm) {

        City city = realm.where(City.class).equalTo("cityName", "Kiev").findFirst();

        if (city != null) {
            Log.d("DEBUG", "Cities items already created");
            return;
        } else {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Number currentIdNum = realm.where(City.class).max("id");
                    int nextId;
                    if (currentIdNum == null) {
                        nextId = 1;
                    } else {
                        nextId = currentIdNum.intValue() + 1;
                    }

                    City cityKiev = realm.createObject(City.class, nextId);
                    //cityKiev.setId(nextId);
                    cityKiev.setCityName("Kiev");
                    cityKiev.setApiCityID(703448);
                    nextId++;

                    City cityLondon = realm.createObject(City.class, nextId);
                    //cityLondon.setId(nextId);
                    cityLondon.setCityName("London");
                    cityLondon.setApiCityID(2643743);
                    nextId++;

                    City cityNewYork = realm.createObject(City.class, nextId);
                    //cityNewYork.setId(nextId);
                    cityNewYork.setCityName("New York");
                    cityNewYork.setApiCityID(5128581);

                }
            });
        }
    }

}
