package com.example.dmitriykosse.cogniance;

/**
 * Created by Dmitriy.Kosse on 10.07.2017.
 */
import android.content.Context;
import android.graphics.Typeface;

public class FontManager {

    private static FontManager instance;

    private FontManager() {
    }

    public static FontManager getInstance() {
        if (instance == null) {
            instance = new FontManager();
        }
        return instance;
    }

    public Typeface getTypeface(Context context, int textStyle) {
        switch (textStyle) {
            case Typeface.NORMAL:
                return Typeface.createFromAsset(context.getAssets(),"fonts/ostrich-regular.ttf");
            case Typeface.BOLD:
                return Typeface.createFromAsset(context.getAssets(),"fonts/Chantelli_Antiqua.ttf");
            case Typeface.ITALIC:
                return Typeface.createFromAsset(context.getAssets(),"fonts/ostrich-regular.ttf");
            case Typeface.BOLD_ITALIC:
                return Typeface.createFromAsset(context.getAssets(),"fonts/Chantelli_Antiqua.ttf");
            default:
                return Typeface.createFromAsset(context.getAssets(),"fonts/Chantelli_Antiqua.ttf");
        }
    }

}
