package com.example.dmitriykosse.cogniance.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Dmitriy.Kosse on 21.07.2017.
 */

public class HistoryData {

    @JsonIgnore
    public String message;

    public String list;

    @JsonIgnore
    public String calctime;

    public Integer city_id;
    public Integer cod;
    public Integer cnt;

    @Override
    public String toString() {
        return String.format("%s %s %s %s", this.city_id, this.cod, this.cnt, this.list);
    }
}
