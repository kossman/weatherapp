package com.example.dmitriykosse.cogniance;

/**
 * Created by Dmitriy.Kosse on 11.07.2017.
 */

public class WeatherApi {

    public static final String appid = "43297dcd2ed8bb47f78a6beb73582bd8";
    public static final String weatherApiBaseUrl = "http://api.openweathermap.org/data/2.5/weather?id=%s&APPID=%s&units=metric";
    public static final String weatherApiHistory = "http://history.openweathermap.org/data/2.5/history/city?id=%s&type=hour&start=%s&end=%s&appid=%s&units=metric";
    public static final String getWeatherApiHistoryExample = "http://samples.openweathermap.org/data/2.5/history/city?id=2885679&type=hour&appid=b1b15e88fa797225412429c1c50c122a1";


    public static Integer cityIdKyiv = 703448;
    public static Integer cityIdLondon = 2643743;
    public static Integer cityIdNewYork = 5128581;


}
